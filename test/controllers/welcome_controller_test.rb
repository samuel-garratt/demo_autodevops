require 'test_helper'

class WelcomeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get welcome_index_url
    assert_response :success
    assert_select "h1", "You're on Rails! Powered by GitLab Auto DevOps."
  end

end
